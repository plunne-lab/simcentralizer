#ifndef HID_COMMANDS_H
#define HID_COMMANDS_H

#include "stdint.h"

typedef struct
{
	uint8_t buttons[4];
	int8_t x;
	int8_t y;
	uint8_t z;
	uint8_t rx;
	uint8_t ry;
	uint8_t rz;
}usbMapping_t;

void pressButton(usbMapping_t *map, uint8_t n);
void releaseButtons(usbMapping_t *map);
void releaseShifter(usbMapping_t *map);

#endif // HID_COMMANDS_H

