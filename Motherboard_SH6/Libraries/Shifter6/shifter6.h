#ifndef SHIFTER6_H
#define SHIFTER6_H

#include "stdint.h"

/*
#define XAXISLOW 	112
#define XAXISHIGH 	162
#define YAXISLOW 	50
#define YAXISHIGH 	200
*/

#define XAXISLOW 	80
#define XAXISHIGH 	150
#define YAXISLOW 	80
#define YAXISHIGH 	150

typedef struct
{
	uint8_t xAxis;
	uint8_t yAxis;
	uint8_t reverse;
}Shifter6_t;

uint8_t getGear(Shifter6_t *s);

#endif // SHIFTER6_H

