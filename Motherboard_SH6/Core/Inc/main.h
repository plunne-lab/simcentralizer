/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LED13_Pin GPIO_PIN_13
#define LED13_GPIO_Port GPIOC
#define SHIFTER6_X_Pin GPIO_PIN_0
#define SHIFTER6_X_GPIO_Port GPIOA
#define SHIFTER6_Y_Pin GPIO_PIN_1
#define SHIFTER6_Y_GPIO_Port GPIOA
#define HANDB_Pin GPIO_PIN_2
#define HANDB_GPIO_Port GPIOA
#define THROTTLE_Pin GPIO_PIN_3
#define THROTTLE_GPIO_Port GPIOA
#define BRAKE_Pin GPIO_PIN_4
#define BRAKE_GPIO_Port GPIOA
#define CLUTCH_Pin GPIO_PIN_5
#define CLUTCH_GPIO_Port GPIOA
#define SEQ_UP_Pin GPIO_PIN_6
#define SEQ_UP_GPIO_Port GPIOA
#define SEQ_DOWN_Pin GPIO_PIN_7
#define SEQ_DOWN_GPIO_Port GPIOA
#define BOX_MODE_Pin GPIO_PIN_0
#define BOX_MODE_GPIO_Port GPIOB
#define BOX_RST_Pin GPIO_PIN_1
#define BOX_RST_GPIO_Port GPIOB
#define SHIFTER6_R_Pin GPIO_PIN_9
#define SHIFTER6_R_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
